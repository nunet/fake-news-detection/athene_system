import sys, os.path as path
from builtins import isinstance
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from pipeline import single_data_run,load_model,generate_features_single_test
import time

from fnc.refs.utils.dataset import DataSet
from fnc.refs.utils.testDataset  import TestDataSet
import csv



class Predict():
    def __init__(self):
        self.data_path = "%s/data/fnc-1" % (path.dirname(path.dirname(path.abspath(__file__))))
        self.splits_dir = "%s/data/fnc-1/splits" % (path.dirname(path.dirname(path.abspath(__file__))))
        self.features_dir="%s/data/fnc-1/features" % (path.dirname(path.dirname(path.abspath(__file__))))
        self.result_file_folder = "%s" % (path.dirname(path.dirname(path.abspath(__file__))))
        self.embeddPath = "%s/data/embeddings/google_news/GoogleNews-vectors-negative300.bin.gz" % (path.dirname(path.dirname(path.abspath(__file__))))
       
        self.Xs = dict()
        self.ys = dict()

        # (scorer_type, [normal features], [non-bleeding features])
        self.feature_list = [
            # ORIGINAL FEATURES OF FNC-1 BEST SUBMISSION 3)
            ('voting_mlps_hard',
             ['overlap',
              'refuting',
              'polarity',
              'hand',
              #'NMF_fit_all_incl_holdout_and_test',
              #'latent_dirichlet_allocation_incl_holdout_and_test',
              #'latent_semantic_indexing_gensim_holdout_and_test',
              #'NMF_fit_all_concat_300_and_test',
              #'word_ngrams_concat_tf5000_l2_w_holdout_and_test',
              'stanford_wordsim_1sent'],
             [])
        ]

        # define and create parent folder to save all trained classifiers into
        self.parent_folder = "%s/data/fnc-1/mlp_models/" % (path.dirname(path.dirname(path.abspath(__file__))))
        # self.fnc_result_folder = "%s/data/fnc-1/fnc_results/" % (path.dirname(path.dirname(path.abspath(__file__))))


        scorer_type = 'voting_mlps_hard'
        # load model [scorer_type]_final_2 classifier
        self.filename = scorer_type + "_final.sav"
        self.load_clf = load_model(self.parent_folder + scorer_type + "_final_new_2/",self.filename)  # TODO set the correct path to the classifier here
        


        print("Load model for final prediction of test set: " + self.parent_folder + scorer_type + "_final_new_2/" + self.filename)

        


    def single_data(self,a_headline, a_body):
        d = DataSet('', a_headline, a_body)

        for scorer_type, features, non_bleeding_features in self.feature_list:
            d = TestDataSet('', a_headline, a_body)

            # generate features for the unlabeled testing set
            X_final_test = generate_features_single_test(d.stances, d, str("final_test"), features, self.features_dir)
        
            # predict classes and turn into labels
            y_predicted = self.load_clf.predict_proba(X_final_test)
            labeled_prediction = dict(zip(["agree", "disagree", "discuss", "unrelated"],
                                    ['{:.2f}'.format(s_c) for s_c in y_predicted[0]]))
            #predicted = [LABELS[int(a)] for a in y_predicted]
            #print("Prediction Length: ", len(predicted))
            print("Prediction Raw: ", labeled_prediction)
            #print("Predicted output: ", predicted)
            return labeled_prediction



class HTTPserver(BaseHTTPRequestHandler):
    def __init__(self):
        self.pr = Predict()

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        content_type = self.headers['Content-Type']
        if content_type == 'application/json':
            data = self.rfile.read(content_length)
            json_data = json.loads(data.decode('utf-8'))
            if list(json_data.keys()).count('headline') == 1 and \
               list(json_data.keys()).count('body') == 1: 
                labeled_pred = self.pr.single_data(json_data['headline'],
                                               json_data['body'])
                self.send_response(200)
                self.send_header('Content-Type', 'text/plain')
                self.end_headers()
                self.wfile.write(json.dumps(dict(labeled_pred)).encode('utf-8'))
            else:
                self.send_response(400)
                self.send_header('Content-Type', 'text/plain')
                self.end_headers()
                self.wfile.write("Input should be json with keys 'headline' and 'body'")
        else:
            self.send_response(400)
            self.send_header('Content-Type', 'text/plain')
            self.end_headers()


if __name__ == '__main__':
    _hd = "Hundreds of Palestinians flee floods in Gaza as Israel opens dams"
    _bd = """Hundreds of Palestinians were evacuated from their homes Sunday morning after Israeli authorities opened a number of dams near the border, flooding the Gaza Valley in the wake of a recent severe winter storm.

    The Gaza Ministry of Interior said in a statement that civil defense services and teams from the Ministry of Public Works had evacuated more than 80 families from both sides of the Gaza Valley (Wadi Gaza) after their homes flooded as water levels reached more than three meters.

    Gaza has experienced flooding in recent days amid a major storm that saw temperatures drop and frigid rain pour down.

    The storm displaced dozens and caused hardship for tens of thousands, including many of the approximately 110,000 Palestinians left homeless by Israel's assault over summer.

    The suffering is compounded by the fact that Israel has maintained a complete siege over Gaza for the last eight years, severely limiting electricity and the availability of fuel for generators. It has also prevented the displaced from rebuilding their homes, as construction materials are largely banned from entering.

    Gaza civil defense services spokesman Muhammad al-Midana warned that further harm could be caused if Israel opens up more dams in the area, noting that water is currently flowing at a high speed from the Israel border through the valley and into the Mediterranean sea.

    Evacuated families have been sent to shelters sponsored by UNRWA, the UN agency for Palestinian refugees, in al-Bureij refugee camp and in al-Zahra neighborhood in the central Gaza Strip.

    The Gaza Valley (Wadi Gaza) is a wetland located in the central Gaza Strip between al-Nuseirat refugee camp and al-Moghraqa. It is called HaBesor in Hebrew, and it flows from two streams -- one whose source runs from near Beersheba, and the other from near Hebron.

    Israeli dams on the river to collect rainwater have dried up the wetlands inside Gaza, and destroyed the only source of surface water in the area.

    Locals have continued to use it to dispose of their waste for lack of other ways to do so, however, creating an environmental hazard.

    This is not the first time Israeli authorities have opened the Gaza Valley dams.

    In Dec. 2013, Israeli authorities also opened the dams amid heavy flooding in the Gaza Strip. The resulting floods damaged dozens of homes and forces many families in the area from their homes.

    In 2010, the dams were opened as well, forcing 100 families from their homes. At the time civil defense services said that they had managed to save seven people who had been at risk of drowning."""
    #pipeline()
    #single_data_run("this is not it", "this is not it")
    if len(sys.argv) == 2:
        server_port = int(sys.argv[1])
        httpd = HTTPServer(('', server_port), HTTPserver)
        try:
            print("Starting REST Server on port: ", str(server_port))
            httpd.serve_forever()
        except KeyboardInterrupt:
            print("Exiting...")
            httpd.server_close()


# title= 'Here when the US will see a lot more vaccine doses'
# body = 'A fourth Covid-19 vaccine could become available in the US in April, when AstraZeneca could secure FDA authorization of its vaccine. Dr. Ruud Dobber, the executive vice president and president of AstraZeneca biopharmaceuticals business unit, said the company will immediately release 30 million doses upon authorization of the vaccine and up to 50 million doses by the end of April.'

# # load the model and call the class when you recive request
# start_time = time.time()
# single_data_run(title,body)
# original = time.time()-start_time


# # load the model when starting the server
# pr = Predict()

# # recive a request and call the class
# start_time = time.time()
# pr.single_data(title,body)
# modefied = time.time()-start_time







# print('----------------------')
# print('priv time',original)
# print('new data',modefied)
